import "./ActionBtnsForModal.scss";

function ActionBtnsForModal({ actions }) {
  return <div className="btnsForModal">{actions}</div>;
}
export default ActionBtnsForModal;
