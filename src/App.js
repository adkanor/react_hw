import { useState } from "react";
import Button from "./components/buttons/Button";
import Modal from "./components/modals/Modal";
import FullScreenWrapper from "./components/wrapper/FullScreenWrapper";

function App() {
  const [modal, setModal] = useState({
    firstModal: false,
    secondModal: false,
  });

  const closeModalWindow = () => {
    setModal({ firstModal: false, secondModal: false });
  };
  const [closeBtnForModal, setCloseBtnForModal] = useState(true);

  return (
    <>
      <FullScreenWrapper>
        <Modal
          active={modal.firstModal}
          closeModal={closeModalWindow}
          header="Do you want to delete this?"
          textContent="Click ok if you want to delete the file. Are you sure you want to delete it?"
          shouldClose={closeBtnForModal}
        >
          <button className="actionForModal submit" onClick={closeModalWindow}>
            OK
          </button>
          <button className="actionForModal cancel" onClick={closeModalWindow}>
            Cancel
          </button>
        </Modal>

        <Modal
          active={modal.secondModal}
          closeModal={closeModalWindow}
          header="Are you sure you want to continue?"
          textContent="If you continue, no info will be saved. Are you sure about this?"
          shouldClose={closeBtnForModal}
        >
          <button className="actionForModal submit" onClick={closeModalWindow}>
            OK
          </button>
          <button className="actionForModal cancel" onClick={closeModalWindow}>
            Cancel
          </button>
        </Modal>

        <Button
          backgroundColor="lightBlue"
          text="Open first modal"
          onClick={() => {
            setModal({ ...modal, firstModal: true });
          }}
        />

        <Button
          backgroundColor="lightGreen"
          text="Open second modal"
          onClick={() => {
            setModal({ ...modal, secondModal: true });
          }}
        />
      </FullScreenWrapper>
    </>
  );
}

export default App;
