import "./Modal.scss";
import ActionBtnsForModal from "../ActionBtnsForModal/ActionBtnsForModal";
function Modal({
  active,
  closeModal,
  header,
  textContent,
  shouldClose,
  children,
}) {
  return (
    <div className={active ? "modal active" : "modal"} onClick={closeModal}>
      <div className="modal_content" onClick={(e) => e.stopPropagation()}>
        <div
          className={
            shouldClose ? "modal-closeButton toClose" : "modal-closeButton"
          }
          onClick={closeModal}
        >
          X
        </div>
        <h2 className="header">{header}</h2>
        <p className="modalTextContent">{textContent}</p>
        <ActionBtnsForModal actions={children} />
      </div>
    </div>
  );
}

export default Modal;
