import "./FullScreenWrapper.scss";

function FullScreenWrapper({ children }) {
  return <div className="fullScreenWrapper">{children}</div>;
}

export default FullScreenWrapper;
