import "./Button.scss";

function Button({ backgroundColor, text, onClick }) {
  const bgColor = {
    backgroundColor: backgroundColor,
  };

  return (
    <button onClick={onClick} className="button" style={bgColor}>
      {text}
    </button>
  );
}

export default Button;
